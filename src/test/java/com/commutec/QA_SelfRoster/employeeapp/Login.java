package com.commutec.QA_SelfRoster.employeeapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class Login {

    AndroidDriver<AndroidElement> driver;
    public Properties props;

    @BeforeTest
    public void launch() throws IOException, InterruptedException {
    //load the properties file
    props=new Properties();
    final FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "//application.properties");
    props.load(objfile);

    final File f = new File("src");
    final File apk = new File(f, "app-release.apk");  

    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
    caps.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
    caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.commuteck.customer");
    caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.commuteck.customer.MainActivity");
    caps.setCapability(MobileCapabilityType.APP,apk.getAbsolutePath());

    // lunch app
    driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
    Thread.sleep(7000);
}
@Test 
public void userlogin() throws InterruptedException {
    driver.findElement(By.xpath(props.getProperty("emp_mobile"))).sendKeys(props.getProperty("emp_login_NO"));
    driver.findElement(By.xpath(props.getProperty("emp_password"))).sendKeys(props.getProperty("emp_login_password"));
    
    // Click back button on android device 
    driver.navigate().back();
    Thread.sleep(2000);
    driver.findElement(By.xpath(props.getProperty("emp_login"))).click();
    Thread.sleep(1000);
    
    //check assert for Agreement pop-up 
    WebElement agreement_pop=driver.findElement(By.xpath(props.getProperty("emp_agreement_scr")));
    if(agreement_pop.isDisplayed()){

    Thread.sleep(3000);

    //Accept Agreement
    driver.findElement(By.xpath(props.getProperty("emp_agreement_Accept"))).click();
    Thread.sleep(7000);

    // Allow Location
    driver.findElement(By.xpath(props.getProperty("empl_location_allow"))).click();
    Thread.sleep(7000);

    //Allow Commutec employee access
    driver.findElement(By.xpath(props.getProperty("emp_Commutec_access"))).click();
    Thread.sleep(5000);
    
    Assert.assertEquals(driver.findElement(By.xpath(props.getProperty("emp_home_scr"))).isDisplayed(),true);
    }
    else{
    Assert.assertEquals(driver.findElement(By.xpath(props.getProperty("emp_home_scr"))).isDisplayed(),true);
    }


    }
@AfterTest
public void close(){
    //driver.quit();
}

}


